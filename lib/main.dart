import 'package:flutter/material.dart';
import './quiz.dart';
import './result.dart';

// void main() {
//   runApp(MyApp());
// }

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  final questions = [
    {
      'questionText': 'The color you like the most?',
      'answer': [
        {'text': 'black', 'score': 10},
        {'text': 'white', 'score': 8},
        {'text': 'blue', 'score': 6},
        {'text': 'green', 'score': 4},
      ]
    },
    {
      'questionText': 'Dream car?',
      'answer': [
        {'text': 'tesla', 'score': 10},
        {'text': 'bmw', 'score': 8},
        {'text': 'benz', 'score': 6},
        {'text': 'toyota', 'score': 4},
      ]
    },
    {
      'questionText': 'Your super hero?',
      'answer': [
        {'text': 'bats', 'score': 10},
        {'text': 'ironGiant', 'score': 8},
        {'text': 'deadpool', 'score': 6},
        {'text': 'sups', 'score': 4},
      ]
    },
  ];

  var questionIndex = 0;
  var _totalScore = 0;

  void resetQuiz() {
    setState(() {
      questionIndex = 0;
      _totalScore = 0;
    });
  }

  void _questionAnswer(int score) {
    _totalScore += score;

    setState(() {
      questionIndex = questionIndex + 1;
    });
    if (questionIndex < questions.length) {
      print(questionIndex);
    }
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: Text("Questions"),
        ),
        body: questionIndex < questions.length
            ? Quiz(
                questions: questions,
                questionAnswer: _questionAnswer,
                questionIndex: questionIndex)
            : Result(_totalScore, resetQuiz),
      ),
    );
  }
}
